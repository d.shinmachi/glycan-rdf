package org.expasy.glycanrdf.neo4j.cypher;

import com.google.common.base.Preconditions;
import org.expasy.glycanrdf.configuration.ConfigurationManager;
import org.neo4j.cypher.javacompat.ExecutionEngine;
import org.neo4j.cypher.javacompat.ExecutionResult;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.openide.util.Lookup;

import java.util.Map;

/**
 * @author jmarieth
 * @author dalocci
 * @version sqrt -1.
 */
public class CypherQueryService {

    private final GraphDatabaseService graphDb;
    private final ExecutionEngine engine;

    public CypherQueryService() {

        ConfigurationManager manager = Lookup.getDefault().lookup(ConfigurationManager.class);
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(manager.getProperty("neo4j.path"));
        engine = new ExecutionEngine( graphDb );
    }

    public ExecutionResult query(String query){

        ExecutionResult result = null;

        Transaction tx = graphDb.beginTx();
        try {

            tx = graphDb.beginTx();
            result = engine.execute(query);

            tx.success();
        } catch(Exception e) {

            throw new IllegalStateException(e);
        } finally {

            tx.close();
        }

        return result;
    }

    public void getMapResult(ExecutionResult executionResult, Map<String, String> mapResult){

        Preconditions.checkNotNull(executionResult);
        Preconditions.checkNotNull(mapResult);

        for ( Map<String, Object> row : executionResult )
        {
            for ( Map.Entry<String, Object> column : row.entrySet() )
            {
                mapResult.put(column.getValue().toString(), "");
            }
        }
    }
}
