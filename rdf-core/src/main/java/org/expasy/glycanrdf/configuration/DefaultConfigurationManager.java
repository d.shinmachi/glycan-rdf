package org.expasy.glycanrdf.configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.Logger;
import org.openide.util.lookup.ServiceProvider;

import java.net.URL;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
@ServiceProvider(service = ConfigurationManager.class)
public class DefaultConfigurationManager implements ConfigurationManager {

    private static final Logger LOGGER = Logger.getLogger(DefaultConfigurationManager.class);
    private XMLConfiguration configuration;
    public static final String PROPERTY_CONFIGURATION = "org.expasy.glycanrdf.configurationFile";

    public DefaultConfigurationManager(){
        try {
            if(System.getProperty(PROPERTY_CONFIGURATION) != null){
                configuration = new XMLConfiguration(System.getProperty(PROPERTY_CONFIGURATION));
            } else {
                URL resource = this.getClass().getResource("configuration.xml");
                configuration = new XMLConfiguration(resource);
            }
            LOGGER.info("Configuration file found");
            FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
            strategy.setRefreshDelay(30000);
            configuration.setReloadingStrategy(strategy);
        } catch (ConfigurationException e) {
            String errorMessage = "Cannot load the configuration file. You can use the property org.expasy.glycanrdf.configurationFile to specify a new configuration file. ";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }
    }


    public void reload(){
        try {
            if(System.getProperty(PROPERTY_CONFIGURATION) != null){
                configuration = new XMLConfiguration(System.getProperty(PROPERTY_CONFIGURATION));
            } else {
                URL resource = this.getClass().getResource("configuration.xml");
                configuration = new XMLConfiguration(resource);
            }
            LOGGER.info("Configuration file found");
        } catch (ConfigurationException e) {
            String errorMessage = "Cannot load the configuration file. You can use the property org.expasy.glycanrdf.configurationFile to specify a new configuration file. ";
            LOGGER.fatal(errorMessage);
            throw new IllegalStateException(errorMessage,e);
        }
    }

    public String getProperty(final String key)
    {
        if(configuration.getProperty(key) instanceof  String){
            return (String) configuration.getProperty(key);
        } else {
            throw new IllegalArgumentException("The property can only be a string "+ key +":"+ configuration.getProperty(key));
        }

    }
}
